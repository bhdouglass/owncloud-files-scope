var fs = require('fs');
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var template = require('gulp-template');
var rename = require('gulp-rename');
var del = require('del');
var i18nextparser = require('i18next-parser');
var stylish = require('jshint-stylish');

var paths = {
    lint: ['src/*.js', 'gulpfile.js'],
    pot: 'src/*.js',
    ini: ['src/data/owncloud-files-settings.ini.in', 'src/data/owncloud-files.ini.in'],
    pot_template: 'po/owncloud-files-scope.pot.in',
    pot_file: 'owncloud-files-scope.pot',
    dist: 'dist',
    locales: 'dist/locales',
    translation: './dist/locales/en_US/translation.json',
};

gulp.task('clean', function() {
    del.sync(paths.dist);
});

gulp.task('lint', function() {
    return gulp.src(paths.lint)
        .pipe(jshint())
        .pipe(jshint.reporter(stylish))
        .pipe(jshint.reporter('fail'));
});

gulp.task('i18next', function() {
    return gulp.src(paths.pot)
        .pipe(i18nextparser({
            locales: ['en_US'],
            functions: ['_'],
            output: paths.locales,
            namespaceSeparator: '|',
            keySeparator: '^',
        }))
        .pipe(gulp.dest(paths.locales));
});

gulp.task('i18next-ini', ['i18next'], function() {
    var translations = JSON.parse(
        fs.readFileSync(paths.translation)
    );

    paths.ini.forEach(function(ini) {
        var file = fs.readFileSync(ini, 'utf8').split('\n');
        file.forEach(function(line) {
            if (line[0] == '_') {
                var split = line.split('=');
                var string = split.slice(1).join('=');

                if (!translations[string]) {
                    translations[string] = '';
                }
            }
        });
    });

    fs.writeFileSync(paths.translation, JSON.stringify(translations, null, 4));
});

gulp.task('pot', ['clean', 'i18next-ini'], function() {
    var content = '';
    var translations = JSON.parse(
        fs.readFileSync(paths.translation)
    );

    for (var key in translations) {
        key = key.replace(/"/g, '\\"');
        content += 'msgid "' + key + '"\nmsgstr ""\n\n';
    }

    return gulp.src(paths.pot_template)
        .pipe(template({
            content: content
        }))
        .pipe(rename(paths.pot_file))
        .pipe(gulp.dest('po'));
});
