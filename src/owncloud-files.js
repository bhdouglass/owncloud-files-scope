var moment = require('moment');
var path = require('path');

var OwnCloud = require('./webdav');
var utils = require('./utils');
var filetypes = require('./filetypes');
var previews = require('./previews');
var translations = require('./translations');
var _ = translations._;

var scopes = require('unity-js-scopes');
var scope = scopes.self; //convenience

var TEMPLATE = JSON.stringify({
    'schema-version': 1,
    'template': {
        'category-layout': 'grid',
        'card-size': 'small',
    },
    'components': {
        'title': 'title',
        'art' : {
            'field': 'art',
        },
    },
});

var ERROR_TEMPLATE = JSON.stringify({
    'schema-version': 1,
    'template': {
        'category-layout': 'grid',
        'card-size': 'medium',
    },
    'components': {
        'title': 'title',
        'art' : {
            'field': 'art',
        },
        'subtitle': 'subtitle',
    },
});

function error_result(search_reply, id, title, subtitle, error_message, category_title) {
    category_title = category_title ? category_title : 'error';
    var category_id = category_title.replace(' ', '-').toLowerCase();

    var category_renderer = new scopes.lib.CategoryRenderer(ERROR_TEMPLATE);
    var category = search_reply.register_category(category_id, category_title, '', category_renderer);

    var result = new scopes.lib.CategorisedResult(category);
    result.set_uri(id);
    result.set_title(title);
    result.set('subtitle', subtitle ? subtitle : '');
    result.set('preview_type', previews.types.ERROR);
    result.set_art(path.join(scope.scope_directory, 'error.svg'));

    if (error_message) {
        result.set('message', error_message);
    }

    return result;
}

var scope_id = null;
var ownCloud = null;

scope.initialize(
    {}, //options
    {
        run: function scope_run() {
            utils.log('running');
        },
        start: function scope_start(id) {
            utils.log('starting (' + id + ')');
            scope_id = id;

            var settings = utils.settings(scope);
            ownCloud = new OwnCloud(settings.url, settings.username, settings.password, settings.allow_self_signed, settings.dav_fallback_mode);
        },
        search: function scope_search(canned_query, metadata) {
            var settings = utils.settings(scope);
            ownCloud.updateSettings(settings.url, settings.username, settings.password, settings.allow_self_signed, settings.dav_fallback_mode);

            return new scopes.lib.SearchQuery(
                canned_query,
                metadata,
                function query_run(search_reply) { //TODO departments based on file types (do this after mimetype checking)
                    var qs = canned_query.query_string();
                    utils.log('search: ' + qs);

                    if (ownCloud.url) {
                        var category_renderer = new scopes.lib.CategoryRenderer(TEMPLATE);
                        var file_category = search_reply.register_category('files', _('Files'), '', category_renderer);
                        var folder_category = search_reply.register_category('folders', _('Folders'), '', category_renderer);
                        var actions_category = search_reply.register_category('actions', _('Actions'), '', category_renderer);

                        var dir = qs || '/';
                        if (dir.charAt(0) != '/') {
                            dir = '/' + dir;
                        }

                        if (dir != '/') {
                            var parent = path.normalize(path.join(dir, '..'));

                            var parent_result = new scopes.lib.CategorisedResult(folder_category);
                            parent_result.set_uri('scope://owncloud-files-scope.bhdouglass_owncloud-files?q=' + utils.sanitize_path(parent));
                            parent_result.set_title(_('Parent Folder'));
                            parent_result.set('path', parent);
                            parent_result.set_intercept_activation();
                            parent_result.set_art(path.join(scope.scope_directory, 'parent.png'));

                            search_reply.push(parent_result);
                        }

                        ownCloud.getDir(dir).then(function(contents) {
                            if (contents.length === 0) {
                                utils.warn('empty folder');

                                search_reply.push(error_result(search_reply, 'empty-folder', _('This folder is empty'), dir, '', _('Empty Folder')));
                                search_reply.finished();
                            }
                            else {
                                var hide_dot = (scope.settings.hide_dot === undefined) ? true : scope.settings.hide_dot.get_bool();
                                var gallery_path = ''; //the preview will run basename on the path, so use an image's full path
                                var playlist_path = '';
                                var gallery = [];
                                var playlist = [];

                                for (var index in contents) {
                                    var file = contents[index];

                                    if (file) {
                                        if (hide_dot && file.basename.charAt(0) == '.') {
                                            continue;
                                        }
                                        else {
                                            if (file.type == 'file') {
                                                var file_result = new scopes.lib.CategorisedResult(file_category);
                                                file_result.set_uri(file.path);
                                                file_result.set_title(file.basename);
                                                file_result.set('preview_type', previews.types.FILE);
                                                file_result.set('path', file.path);
                                                file_result.set('mtime', moment(new Date(file.lastmod)).fromNow());
                                                file_result.set('size', utils.nice_bytes(file.size));
                                                file_result.set('mimetype', file.mimetype);

                                                //TODO do this checking based on mimetype && move it into the filetypes.js file
                                                if (filetypes.text.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_text.png'));
                                                }
                                                else if (filetypes.doc.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_doc.png'));
                                                }
                                                else if (filetypes.preview_image.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(utils.download_link(ownCloud.external_url, file.path));
                                                    gallery.push(utils.download_link(ownCloud.external_url, file.path));
                                                    gallery_path = file.path;
                                                }
                                                else if (filetypes.image.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_image.png'));
                                                }
                                                else if (filetypes.preview_video.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_movie.png'));
                                                    file_result.set('preview_type', previews.types.VIDEO);
                                                }
                                                else if (filetypes.video.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_movie.png'));
                                                }
                                                else if (filetypes.preview_audio.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_sound.png'));
                                                    file_result.set('preview_type', previews.types.AUDIO);
                                                    playlist.push(utils.download_link(ownCloud.external_url, file.path));
                                                    playlist_path = file.path;
                                                }
                                                else if (filetypes.audio.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_sound.png'));
                                                }
                                                else if (filetypes.archive.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_zip.png'));
                                                }
                                                else if (filetypes.pdf.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_pdf.png'));
                                                }
                                                else if (filetypes.code.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_code.png'));
                                                }
                                                else if (filetypes.powerpoint.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_ppt.png'));
                                                }
                                                else if (filetypes.spreadsheet.indexOf(file.ext) >= 0) {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file_xls.png'));
                                                }
                                                else {
                                                    file_result.set_art(path.join(scope.scope_directory, 'file.png'));
                                                }

                                                search_reply.push(file_result);
                                            }
                                            else {
                                                var folder_result = new scopes.lib.CategorisedResult(folder_category);
                                                folder_result.set_uri('scope://owncloud-files-scope.bhdouglass_owncloud-files?q=/' + utils.sanitize_path(file.path));
                                                folder_result.set_title(file.basename);
                                                folder_result.set('path', file.path);
                                                folder_result.set('mtime', moment(new Date(file.lastmod)).fromNow());
                                                folder_result.set_intercept_activation();
                                                folder_result.set_art(path.join(scope.scope_directory, 'folder.png'));

                                                search_reply.push(folder_result);
                                            }
                                        }
                                    }
                                }

                                if (gallery.length > 0) {
                                    var gallery_result = new scopes.lib.CategorisedResult(actions_category);
                                    gallery_result.set_uri(dir + '#gallery');
                                    gallery_result.set_title(_('Gallery'));
                                    gallery_result.set_art(path.join(scope.scope_directory, 'file_image.png'));
                                    gallery_result.set('preview_type', previews.types.GALLERY);
                                    gallery_result.set('path', gallery_path);
                                    gallery_result.set('gallery', JSON.stringify(gallery));

                                    search_reply.push(gallery_result);

                                    var gallery_shuffle_result = new scopes.lib.CategorisedResult(actions_category);
                                    gallery_shuffle_result.set_uri(dir + '#gallery-shuffle');
                                    gallery_shuffle_result.set_title(_('Shuffled Gallery'));
                                    gallery_shuffle_result.set_art(path.join(scope.scope_directory, 'file_image.png'));
                                    gallery_shuffle_result.set('preview_type', previews.types.GALLERY);
                                    gallery_shuffle_result.set('path', gallery_path);
                                    gallery_shuffle_result.set('gallery', JSON.stringify(gallery));
                                    gallery_shuffle_result.set('shuffle', true);

                                    search_reply.push(gallery_shuffle_result);
                                }

                                if (playlist.length > 0) {
                                    var playlist_result = new scopes.lib.CategorisedResult(actions_category);
                                    playlist_result.set_uri(dir + '#playlist');
                                    playlist_result.set_title(_('Playlist'));
                                    playlist_result.set_art(path.join(scope.scope_directory, 'file_sound.png'));
                                    playlist_result.set('preview_type', previews.types.PLAYLIST);
                                    playlist_result.set('path', playlist_path);
                                    playlist_result.set('playlist', JSON.stringify(playlist));

                                    search_reply.push(playlist_result);

                                    var shuffle_result = new scopes.lib.CategorisedResult(actions_category);
                                    shuffle_result.set_uri(dir + '#shuffle');
                                    shuffle_result.set_title(_('Shuffled Playlist'));
                                    shuffle_result.set_art(path.join(scope.scope_directory, 'file_sound.png'));
                                    shuffle_result.set('preview_type', previews.types.PLAYLIST);
                                    shuffle_result.set('path', playlist_path);
                                    shuffle_result.set('playlist', JSON.stringify(playlist));
                                    shuffle_result.set('shuffle', true);

                                    search_reply.push(shuffle_result);
                                }

                                utils.log('finished searching');
                                search_reply.finished();
                            }
                        })
                        .catch(function(err) {
                            utils.error('error reading dir: ' + err.message);

                            if (err.code == 401) {
                                search_reply.push(error_result(search_reply, 'unauthorized', _('You are not properly logged in'), _('Check the settings'), err.message));
                            }
                            else if (err.code == 403) {
                                search_reply.push(error_result(search_reply, 'forbidden', _('You do not have access to this folder'), dir, err.message));
                            }
                            else if (err.code == 404) {
                                search_reply.push(error_result(search_reply, 'not-found', _('Folder not found'), dir, err.message));
                            }
                            else {
                                search_reply.push(error_result(search_reply, 'list-error', _('Error reading folder'), dir, err.message));
                            }

                            search_reply.finished();
                        });
                    }
                    else { //No connection put an error result
                        search_reply.push(error_result(search_reply, 'no-connection', _('No connection to ownCloud'), _('Check the scope settings')));
                        search_reply.finished();
                    }
                },
                function query_cancelled() {}
            );
        },
        preview: function scope_preview(result, action_metadata) {
            return new scopes.lib.PreviewQuery(
                result,
                action_metadata,
                function preview_run(preview_reply) {
                    previews.preview(preview_reply, result, ownCloud.external_url);
                    preview_reply.finished();
                },
                function preview_cancelled() {}
            );
        },
        activate: function scope_activate(result, metadata) {
            //Force the url dispatcher to take care of things
            utils.log('activating: ' + result.uri());

            return new scopes.lib.ActivationQuery(
                result,
                metadata,
                '',
                '',
                function activate_run() {
                    return new scopes.lib.ActivationResponse(scopes.defs.ActivationResponseStatus.NotHandled);
                },
                function activate_cancelled() {}
            );
        },
        perform_action: function scope_perform_action(result, metadata, widget_id, action_id) {
            //Force the url dispatcher to take care of things
            utils.log('performing action: ' + action_id);

            return new scopes.lib.ActivationQuery(
                result,
                metadata,
                widget_id,
                action_id,
                function perform_action_run() {
                    return new scopes.lib.ActivationResponse(scopes.defs.ActivationResponseStatus.NotHandled);
                },
                function perform_action_cancelled() {}
            );
        }
    }
);
