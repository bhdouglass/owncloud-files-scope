function nice_bytes(bytes) {
    var unit = 'B';

    if (!bytes) {
        bytes = 0;
    }
    else if (bytes > 1024) {
        bytes /= 1024;
        unit = 'KB';

        if (bytes > 1024) {
            bytes /= 1024;
            unit = 'MB';

            if (bytes > 1024) {
                bytes /= 1024;
                unit = 'GB';

                if (bytes > 1024) {
                    bytes /= 1024;
                    unit = 'TB';
                }
            }
        }
    }

    return bytes.toFixed(1) + ' ' + unit;
}

function sanitize_path(path) {
    var parts = path.split('/');
    path = '';
    for (var index in parts) {
        if (path.length > 0) {
            path += '/';
        }

        path += encodeURIComponent(parts[index]);
    }

    if (path[path.length - 1] !== '/') {
        path += '/';
    }

    return path;
}

function download_link(base, path) {
    var url = base;
    if (url[url.length - 1] === '/') {
        url = url.substring(0, url.length - 1);
    }

    return url + path;
}

//Borrowed from http://stackoverflow.com/a/6274381
function shuffle(a) {
    console.log('shuffle');
    var j, x, i;
    for (i = a.length; i; i -= 1) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function log(message) {
    console.log('ownCloud-files-scope.bhdouglass LOG:', message);
}

function warn(message) {
    console.log('ownCloud-files-scope.bhdouglass WARN:', message);
}

function error(message) {
    console.log('ownCloud-files-scope.bhdouglass ERROR:', message);
}

function settings(scope) {
    return {
        url: scope.settings.url ? scope.settings.url.get_string() : '',
        username: scope.settings.username ? scope.settings.username.get_string() : '',
        password: scope.settings.password ? scope.settings.password.get_string() : '',
        allow_self_signed: (scope.settings.allow_self_signed === undefined) ? false : scope.settings.allow_self_signed.get_bool(),
        dav_fallback_mode: (scope.settings.dav_fallback_mode === undefined) ? false : scope.settings.dav_fallback_mode.get_bool(),
    };
}

module.exports = {
    nice_bytes: nice_bytes,
    sanitize_path: sanitize_path,
    download_link: download_link,
    shuffle: shuffle,
    log: log,
    warn: warn,
    error: error,
    settings: settings,
};
