var utils = require('./utils');
var translations = require('./translations');
var _ = translations._;

var fetch = require('node-fetch');
var xml2js = require('xml2js');
var nutil = require('util');
var path = require('path');
var https = require('https');

function MalformedResponseError() {
    Error.call(this);
    this.message = _('Unrecognizable response from the server');
}

nutil.inherits(MalformedResponseError, Error);

function UnauthorizedError() {
    Error.call(this);
    this.message = _('You do not have access to this service');
    this.code = 401;
}

nutil.inherits(UnauthorizedError, Error);

function ForbiddenError() {
    Error.call(this);
    this.message = _('You are not allowed to access this');
    this.code = 403;
}

nutil.inherits(ForbiddenError, Error);

function NotFoundError() {
    Error.call(this);
    this.message = _('The folder you are trying to access does not exist');
    this.code = 404;
}

nutil.inherits(NotFoundError, Error);

function File(filepath, lastmod, size, mimetype) {
    this.type = 'file';
    this.path = decodeURIComponent(filepath);
    this.basename = path.basename(this.path);
    this.ext = path.extname(this.path);
    this.lastmod = lastmod;
    this.size = size ? parseInt(size) : 0;
    this.mimetype = mimetype;
}

function Folder(folderpath, lastmod, size) {
    this.type = 'folder';
    this.path = decodeURIComponent(folderpath);
    this.basename = path.basename(this.path);
    this.lastmod = lastmod;
    this.size = size ? parseInt(size) : 0;
}

function OwnCloud(url, username, password, allow_self_signed, dav_fallback_mode) {
    this.version = null;
    this.updateSettings(url, username, password, allow_self_signed, dav_fallback_mode);
}

OwnCloud.prototype.updateSettings = function(url, username, password, allow_self_signed, dav_fallback_mode) {
    if (url.toLowerCase().indexOf('http://') !== 0 && url.toLowerCase().indexOf('https://') !== 0) {
        url = 'http://' + url;
    }

    //Strip out any webdav related stuff
    url = url.replace('/remote.php/webdav/', '');
    url = url.replace('/remote.php/webdav', '');
    url = url.replace('/remote.php/dav/files/' + username + '/');
    url = url.replace('/remote.php/dav/files/' + username);
    url = url.replace('/remote.php/dav/files/');
    url = url.replace('/remote.php/dav/files');
    url = url.replace('/remote.php/dav/');
    url = url.replace('/remote.php/dav');
    url = url.replace('/remote.php/');
    url = url.replace('/remote.php');

    if (url[url.length - 1] !== '/') {
        url += '/';
    }

    //Compensate for scope bug: https://bugs.launchpad.net/ubuntu/+source/unity-api/+bug/1552082
    if (username[0] === '"' && username[username.length - 1] === '"') {
        username = username.slice(1, username.length - 1);
    }

    if (password[0] === '"' && password[password.length - 1] === '"') {
        password = password.slice(1, password.length - 1);
    }

    //Borrowed from https://github.com/perry-mitchell/webdav-fs/blob/master/source/index.js#L22
    var external_url = (username.length > 0) ? url.replace(/(https?:\/\/)/i, '$1' + encodeURIComponent(username) + ':' + encodeURIComponent(password) + '@') : url;
    if (external_url[external_url.length - 1] !== '/') {
        external_url += '/';
    }

    this.agent = null;
    if (allow_self_signed) {
        this.agent = new https.Agent({
            rejectUnauthorized: false
        });
    }

    if (url != this.url || dav_fallback_mode != this.dav_fallback_mode) {
        this.version = null;
        this.url = url;
        this.external_url = external_url;
        this.dav_url = url;
        this.status_url = url + 'status.php';
    }

    this.username = username;
    this.password = password;
    this.auth = 'Basic ' + new Buffer(username + ':' + password).toString('base64');
    this.dav_fallback_mode = dav_fallback_mode;
};

OwnCloud.prototype.getVersion = function() {
    var self = this;

    return fetch(this.status_url, {
        headers: {
            Authorization: this.auth,
        },
        agent: this.agent,
    }).then(function(res) {
        //TODO error handling

        return res.text();
    }).then(function(text) {
        var json = {};
        try {
            json = JSON.parse(text);
        }
        catch (e) {
            json = {};
        }

        self.version = json.version ? json.version : null;
        if (self.version !== null) {
            utils.log('Found ownCloud v' + self.version);
            if (self.version >= '9' && !self.dav_fallback_mode) { //Version is a string
                self.dav_path = 'remote.php/dav/files/' + self.username;
            }
            else {
                self.dav_path = 'remote.php/webdav';
            }

            self.dav_url += self.dav_path;
            self.external_url += self.dav_path;
        }

        return self.version;
    });
};

OwnCloud.prototype._getDir = function(remote_path) {
    var self = this;
    //TODO throw error if this.version === null

    remote_path = utils.sanitize_path(remote_path);
    if (remote_path.length <= 0) {
        remote_path = '/';
    }
    else if (remote_path[0] != '/') {
        remote_path = '/' + remote_path;
    }

    return fetch(this.dav_url + remote_path, {
        method: 'PROPFIND',
        headers: {
            Depth: 1,
            Authorization: this.auth,
        },
        agent: this.agent,
    }).then(function(res) {
        if (res.status == 401) {
            throw new UnauthorizedError();
        }
        else if (res.status == 403) {
            throw new ForbiddenError();
        }
        else if (res.status == 404) {
            throw new NotFoundError();
        }
        else if (res.status < 200 || res.status > 299) {
            utils.warn('Non-OK response (' + res.status + '): ' + JSON.stringify(res));
        }

        return res.text();
    })
    .then(function(body) {
        var parser = new xml2js.Parser({
            normalizeTags: true,
            explicitArray: false
        });

        return new Promise(function(resolve, reject) {
            parser.parseString(body, function(err, result) {
                if (err) {
                    reject(err);
                }
                else {
                    if (result && result['d:multistatus'] && result['d:multistatus']['d:response']) {
                        var dir = [];

                        for (var index in result['d:multistatus']['d:response']) {
                            var response_item = result['d:multistatus']['d:response'][index];

                            if (response_item && response_item['d:propstat'] && response_item['d:propstat']['d:prop']) {
                                var props = response_item['d:propstat']['d:prop'];

                                var filepath = '';
                                if (response_item['d:href']) {
                                    var pos = response_item['d:href'].indexOf(self.dav_path) + self.dav_path.length;
                                    filepath = response_item['d:href'].substring(pos);
                                }

                                var check_remote_path = decodeURIComponent(remote_path);
                                var check_path = decodeURIComponent(filepath);

                                if (['/webdav', check_remote_path, '/' + check_remote_path, '.', '..', null, ''].indexOf(check_path) == -1) { //Skip over potentially problamatic names
                                    var item = null;
                                    if (props && props['d:resourcetype'] && props['d:resourcetype'] && props['d:resourcetype']['d:collection'] === '') {
                                        item = new Folder(filepath, props['d:getlastmodified'], props['d:quota-used-bytes']);
                                    }
                                    else {
                                        item = new File(filepath, props['d:getlastmodified'], props['d:getcontentlength'], props['d:getcontenttype']);
                                    }

                                    dir.push(item);
                                }
                            }
                        }

                        //Sort folders to be at the top
                        dir = dir.sort(function(a, b) {
                            if (a.type == 'file' && b.type == 'folder') {
                                return 1;
                            }
                            else if (a.type == 'folder' && b.type == 'file') {
                                return -1;
                            }
                            else if (a.basename.toLowerCase() < b.basename.toLowerCase()) {
                                return -1;
                            }
                            else if (a.basename.toLowerCase() > b.basename.toLowerCase()) {
                                return 1;
                            }

                            return 0;
                        });

                        resolve(dir);
                    }
                    else {
                        reject(new MalformedResponseError());
                    }
                }
            });
        });
    });
};

OwnCloud.prototype.getDir = function(remote_path) {
    var self = this;

    if (this.version === null) {
        return this.getVersion().then(function() {
            return self._getDir(remote_path);
        });
    }
    else {
        return this._getDir(remote_path);
    }
};

module.exports = OwnCloud;
