var utils = require('./utils');
var translations = require('./translations');
var _ = translations._;

var path = require('path');
var scopes = require('unity-js-scopes');

function add_header(preview_reply, result) {
    var image = new scopes.lib.PreviewWidget('image', 'image');
    image.add_attribute_mapping('source', 'art');

    var header = new scopes.lib.PreviewWidget('header', 'header');
    header.add_attribute_mapping('title', 'title');
    header.add_attribute_value('subtitle', path.dirname(result.get('path')));

    preview_reply.push([image, header]);
}

function add_mtime(preview_reply, result) {
    if (result.get('mtime')) {
        var mtime = new scopes.lib.PreviewWidget('mtime', 'text');
        mtime.add_attribute_value('text', _('Last Modified') + ': ' + result.get('mtime'));

        preview_reply.push([mtime]);
    }
}

function add_size(preview_reply, result) {
    if (result.get('size')) {
        var size = new scopes.lib.PreviewWidget('size', 'text');
        size.add_attribute_value('text', _('File Size') + ': ' + result.get('size'));

        preview_reply.push([size]);
    }

}

function add_download(preview_reply, result, url) {
    var download = new scopes.lib.PreviewWidget('actions', 'actions');
    download.add_attribute_value('actions',[
        {
            id: 'download',
            label: _('Download'),
            uri: utils.download_link(url, result.get('path')),
        }
    ]);

    preview_reply.push([download]);
}

function error_preview(preview_reply, result) {
    var error_image = new scopes.lib.PreviewWidget('image', 'image');
    error_image.add_attribute_mapping('source', 'art');

    var error_header = new scopes.lib.PreviewWidget('header', 'header');
    error_header.add_attribute_mapping('title', 'title');
    error_header.add_attribute_mapping('subtitle', 'subtitle');

    preview_reply.push([error_image, error_header]);

    if (result.get('message')) {
        var message = new scopes.lib.PreviewWidget('message', 'text');
        message.add_attribute_value('text', _('Error Message') + ': ' + result.get('message'));

        preview_reply.push([message]);
    }
}

function file_preview(preview_reply, result, url) {
    add_header(preview_reply, result);
    add_mtime(preview_reply, result);
    add_size(preview_reply, result);
    add_download(preview_reply, result, url);
}

function video_preview(preview_reply, result, url) {
    add_header(preview_reply, result);
    add_mtime(preview_reply, result);
    add_size(preview_reply, result);
    add_download(preview_reply, result, url);

    //TODO figure out why this doesn't work
    /*
    var video = new scopes.lib.PreviewWidget('video', 'video');
    video.add_attribute_value('source', utils.download_link(url, result.get('path')));

    preview_reply.push([video, header]);
    */
}

function gallery_preview(preview_reply, result) {
    var header = new scopes.lib.PreviewWidget('header', 'header');
    header.add_attribute_mapping('title', 'title');
    header.add_attribute_value('subtitle', path.dirname(result.get('path')));

    var gallery = new scopes.lib.PreviewWidget('gallery', 'gallery');
    var images = JSON.parse(result.get('gallery'));
    if (result.get('shuffle')) {
        utils.shuffle(images);
    }

    gallery.add_attribute_value('sources', images);

    preview_reply.push([header, gallery]);
}

function playlist_preview(preview_reply, result) {
    var header = new scopes.lib.PreviewWidget('header', 'header');
    header.add_attribute_mapping('title', 'title');
    header.add_attribute_value('subtitle', path.dirname(result.get('path')));

    var playlist = JSON.parse(result.get('playlist'));
    playlist.sort(function compare(a, b) {
        var value = 0;
        if (a.toLowerCase() < b.toLowerCase()) {
            value = -1;
        }
        else if (a.toLowerCase() > b.toLowerCase()) {
            value = 1;
        }

        return value;
    });

    var tracks = [];
    for (var index in playlist) {
        tracks.push({
            'title': path.basename(playlist[index]),
            'source': playlist[index],
        });
    }

    if (result.get('shuffle')) {
        utils.shuffle(tracks);
    }

    var playlist_audio = new scopes.lib.PreviewWidget('audio', 'audio');
    playlist_audio.add_attribute_value('tracks', tracks);

    preview_reply.push([header, playlist_audio]);
}

function audio_preview(preview_reply, result, url) {
    add_header(preview_reply, result);

    var audio = new scopes.lib.PreviewWidget('audio', 'audio');
    audio.add_attribute_value(
        'tracks',
        {
            'title': result.title(),
            'source': utils.download_link(url, result.get('path')),
        }
    );

    preview_reply.push([audio]);

    add_mtime(preview_reply, result);
    add_size(preview_reply, result);
    add_download(preview_reply, result, url);
}

var types = {
    ERROR: 'ERROR',
    FILE: 'FILE',
    VIDEO: 'VIDEO',
    GALLERY: 'GALLERY',
    PLAYLIST: 'PLAYLIST',
    AUDIO: 'AUDIO',
};

module.exports = {
    types: types,

    preview: function preview(preview_reply, result, url) {
        if (result.get('preview_type') == types.ERROR) {
            error_preview(preview_reply, result, url);
        }
        else if (result.get('preview_type') == types.VIDEO) {
            video_preview(preview_reply, result, url);
        }
        else if (result.get('preview_type') == types.GALLERY) {
            gallery_preview(preview_reply, result, url);
        }
        else if (result.get('preview_type') == types.PLAYLIST) {
            playlist_preview(preview_reply, result, url);
        }
        else if (result.get('preview_type') == types.AUDIO) {
            audio_preview(preview_reply, result, url);
        }
        else /*if (result.get('preview_type') == types.FILE)*/ {
            file_preview(preview_reply, result, url);
        }
    },
};
