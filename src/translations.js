var fs = require('fs');
var path = require('path');
var Gettext = require('node-gettext');
var gt = new Gettext();

if (process.env.LANG) {
    var lang = process.env.LANG.replace('.UTF-8', '').replace('.utf-8', '');
    var short_lang = lang.split('_')[0];

    var file = path.join(__dirname, '../share/locale/owncloud-files-scope-' + lang + '/LC_MESSAGES/owncloud-files-scope.mo');
    var short_file = path.join(__dirname, '../share/locale/owncloud-files-scope-' + short_lang + '/LC_MESSAGES/owncloud-files-scope.mo');

    if (fs.existsSync(file)) {
        gt.addTextdomain(lang, fs.readFileSync(file));
        gt.textdomain(lang);
    }
    else if (fs.existsSync(short_file)) {
        gt.addTextdomain(short_lang, fs.readFileSync(short_file));
        gt.textdomain(short_lang);
    }
}

module.exports = {
    _: gt.gettext.bind(gt),
    gt: gt,
};
