# ownCloud Files Scope

This is an unofficial ownCloud file scope for Ubuntu Touch.
This scope is not affiliated with or endorsed by ownCloud.

[Install the scope on your ubuntu phone!](https://uappexplorer.com/app/owncloud-files-scope.bhdouglass)

## Translations

Help translate this scope on [Launchpad](https://translations.launchpad.net/owncloud-files-scope)

## Icons

The file and folder icons are from the ownCloud Android app (GPL v2).

The ownCloud logo is from ownCloud Core (AGPL).

The error icon is from Font Awesome (SIL OFL).

## License

Copyright (C) 2016 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
